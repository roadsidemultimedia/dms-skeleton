!function ($) {


	// --> Initialize
	$(document).ready(function() {

		$(document).trigger( 'sectionStart' )

		$.plCommon.init()

		$.plAnimate.initAnimation()

		$('.pl-credit').show()

		$.BodyClass.init()
		
		// Master resize trigger
		$(window).trigger('resize')

	})


	function getPLFixedHeight(){
		
		return $('.pl-fixed-top').height() + $('#wpadminbar').height() + $('.pl-toolbox').height()
		
	}

	function shuffle(o){ //v1.0
		for(var j, x, i = o.length; i; j = parseInt(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
		return o;
	};

	function plRandSort(c) {
	    var o = new Array();
	    for (var i = 0; i < c; i++) {
			o.push(i);

	    }
	    return shuffle(o);
	}


	$(window).load(function() {
		$.plCommon.plVerticalCenter('.pl-centerer', '.pl-centered')
	})

	$.BodyClass = {
		init: function(){
			$(window).resize(function () {
				$.BodyClass.doResize()
			})
			$.BodyClass.doResize()
		}
		,	doResize: function(){
			$(document.body).removeClass('pl-res-phone pl-res-tablet pl-res-desktop')
			var width = $(window).width()		
			if( width < 480 ) {
				$(document.body).addClass('pl-res-phone')
			} else if( width < 1024 ){
				$(document.body).addClass('pl-res-tablet')
			} else {
				$(document.body).addClass('pl-res-desktop')
			}
		} 
	}

	$.plAnimate = {
		
		initAnimation: function(){
			
			var that = this
						
			$.plAnimate.plWaypoints()
			
		}
		
		, plWaypoints: function(selector, options_passed){
			
			var defaults = { 
					offset: '85%' // 'bottom-in-view' 
					, triggerOnce: true
				}
				, options  = $.extend({}, defaults, options_passed)
				, delay = 150
				
			$('.pl-animation-group')
				.find('.pl-animation')
				.addClass('pla-group')
				
			$('.pl-animation:not(.pla-group)').each(function(){

				var element = $(this)

				element.appear(function() {

				  	if( element.hasClass('pl-slidedown') ){

						var endHeight = element.find('.pl-end-height').outerHeight()
						
						element.css('height', endHeight)

					}


				 	$(this)
						.addClass('animation-loaded')
						.trigger('animation_loaded')

				})

			})
				
			$('.pl-animation-group').each(function(){
				
				var element = $(this)

				element.appear(function() {
					
					var animationNum = $(this).find('.pl-animation').size()
					,	randomLoad = plRandSort(animationNum)
					
					
					
				   	$(this)
						.find('.pl-animation')
						.each( function(i){
							var element = $(this)
							
							setTimeout(
								function(){ 
									element
										.addClass('animation-loaded hovered')
									
									setTimeout(function(){ 
										element.removeClass('hovered');
									}, 700); 
								}
								, (i * 200)
							);
						})

				})

			})

			
			
			$('.pl-counter').each(function(){
				
				var cntr = $(this)
				
				cntr.appear( function() {
			
					var the_number = parseInt( cntr.text() )
				
					cntr.countTo({
							from: 0
						,	to: the_number
						,	speed: 2000
						,	refreshInterval: 30
						, 	formatter: function( value, options){
							
							value = Math.round( value )
							var n =  value.toString()
							
							n = n.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
						
							return n
						}
					})
				
				})
				
			})
		}
		
	}

	$.plCommon = {

		init: function(){
			var that = this
			that.setFixedHeight()

			var fixedTop = $('.pl-fixed-top')
			, 	fixedOffset = ( plIsset( fixedTop[0] ) ) ? fixedTop[0].offsetTop : 0
			
			$(document).on('ready scroll', function() {
			    var docScroll = $(document).scrollTop()

			    if (docScroll >= fixedOffset) {
			        fixedTop.addClass('is-fixed');
			 		fixedTop.removeClass('is-not-fixed');
			    } else {
					fixedTop.addClass('is-not-fixed');
			        fixedTop.removeClass('is-fixed')
			    }

			})
			
			$('.pl-make-link').on('click', function(){
				var url = $(this).data('href') || '#'
				, 	newWindow = $(this).attr('target') || false
				
				if( newWindow )
					window.open( url, newWindow )
				else
					window.location.href = url
			
			})
			
			that.handleSearchfield()
			

		}
		
		, handleSearchfield: function(){
			
		
			
			$('.searchfield').on('focus', function(e){
				
				$(this).parent().parent().addClass('has-focus')
					
			}).on( 'blur', function(e){
				
				$(this).parent().parent().removeClass('has-focus')
			
			})
			
			$('.pl-searcher').on('click touchstart', function(e){
				
				e.stopPropagation()
				
				var searchForm = $(this)
				
				$(this).addClass('has-focus').parent().find( '.searchfield' ).focus()
				
				$('body').on('click touchstart', function(e){
					searchForm.removeClass('has-focus')
				})
			})
			
		}

		, setFixedHeight: function(){

			var height = $('.pl-fixed-top').height()

			$('.fixed-top-pusher').height(height)

		}
		
		, plVerticalCenter: function( container, element, offset ) {

			jQuery( container ).each(function(){

				var colHeight = jQuery(this).height()
				,	centeredElement = jQuery(this).find( element )
				,	infoHeight = centeredElement.height()
				, 	offCenter = offset || 0

				centeredElement.css('margin-top', ((colHeight / 2) - (infoHeight / 2 )) + offCenter )
			})

		}
		

	}

}(window.jQuery);