<?php


/* ==================================================================
 * Fix for WP Engine 502 Gateway error 
 * Clear paths stored in database if saved plSectionArea dir doesn't match current dir
 */
$editor_sections_data = get_theme_mod('editor-sections-data');
$stored_section_dir = $editor_sections_data['parent']['PLSectionArea']['base_dir'];
$current_section_dir = trailingslashit(get_template_directory()).'dms/sections/pl_area';
if( $stored_section_dir !== $current_section_dir ){
  set_theme_mod( 'editor-sections-data', array() );
}
/* ================================================================== */

remove_action( 'wp_head', 'pl_scripts_on_ready' );

add_action('wp_enqueue_scripts', 'deregister_dms_scripts', 100);
function deregister_dms_scripts() {
	// wp_dequeue_script('pagelines-fitvids');
	wp_dequeue_script('flexslider');
	wp_dequeue_style('master_font_import');
	// wp_dequeue_style('pagelines-common');
}
add_action('wp_enqueue_scripts', 'enqueue_dms_scripts');
function enqueue_dms_scripts(){
	wp_enqueue_script( 'pagelines-common', get_bloginfo ('stylesheet_directory') . '/assets/js/pl.common.js', array( 'jquery' ), pl_get_cache_key(), true );
	wp_enqueue_script( 'pagelines-helpers', get_bloginfo ('stylesheet_directory') . '/assets/js/pl.helpers.js', array( 'jquery' ), pl_get_cache_key(), true );
	// wp_enqueue_script( 'deep-profiling', 'https://gist.githubusercontent.com/hmert/1546548/raw/74065da859f5caa833745b77820e7697358aa8dc/jquery-profile.js', array( 'jquery' ), pl_get_cache_key(), true );
}

remove_shortcode( 'pl_karma' );
add_shortcode( 'pl_karma', '__return_false' );

// Prevent images from being wrapped in P tags. 
// Particularly useful for blog posts that start with an image. Removes the weird space a the beginning.
function filter_ptags_on_images($content){
 return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');


// add_filter('pl_theme_classes', function($theme_classes){
//   $theme_classes['this_is_amazing'] = 'this is amazing';
//   return array_unique($theme_classes);
// });

// add_filter('pl-area-inner-style', function($var){
//   $var = "shazbot";
//   return $var;
// });

// add_filter('pagelines_lesscode', function($less){
// 	// $less .= pl_file_get_contents( dirname( __FILE__ ) . '/example.less' );
// 	$less .= '.section-test{color:#f90;}';
// 	return $less;
// });

// add_filter('pagelines_insert_core_less', function($less){
//   $less .= '.core-test{color:#f90;}';
//   return $less;
// });

// Load Framework //
require_once( dirname(__FILE__) . '/setup.php' );

class dmsSkeletonTheme {

	public $default_font_types = array(
		array(
			'id' => 'sans-serif',
			'default-family' => '"Helvetica Neue", Arial, sans-serif',
		),
		array(
			'id' => 'sans-serif-alt',
			'default-family' => 'Helvetica',
		),
		array(
			'id' => 'serif',
			'default-family' => 'Georgia, serif',
		),
		array(
			'id' => 'serif-alt',
			'default-family' => 'Georgia, serif',
		),
		array(
			'id' => 'condensed-serif',
			'default-family' => 'Georgia, serif',
		),
		array(
			'id' => 'condensed-sans-serif',
			'default-family' => 'Impact',
		),
		array(
			'id' => 'slab-serif',
			'default-family' => '"Rockwell Extra Bold", "Rockwell Bold", monospace',
		),
		array(
			'id' => 'script',
			'default-family' => '"Brush Script MT", cursive',
		),
		array(
			'id' => 'decorative',
			'default-family' => 'Papyrus, fantasy',
		),
	);

	function __construct() {

		add_action( 'after_setup_theme', array(&$this, 'register_widgets' ), 11 );
		add_filter( 'pless_vars', array(&$this,'custom_less_vars') );
		add_filter( 'pagelines_insert_core_less', array(&$this, 'custom_less_output' ) );

		$this->color_data = $this->get_colors();
		$this->init();
		$this->options();

	}

	function init() {
		require_once( dirname(__FILE__) . '/lib/shortcodes.php' );
	}

	public function custom_less_output($less_output){

		// Color classes
		foreach ($this->color_data['less_output'] as $var => $value) {
			$less_output .= $value;
		}

		return $less_output;
	}

	// Custom LESS Vars
	function custom_less_vars($less){

		global $sms_utils;
		global $sms_redux;
		
		// $this->startTime = microtime(true);

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// Typography variables
		//////////////////////////////////////////////////////////////////////////////////////////////////////

		if($sms_utils){
			$post_id = $sms_utils->global_selected_font_kit_id;
			$active_font_types = ( $sms_utils ? $sms_utils->get_available_font_types( $post_id ) : array() );
		} else {
			$active_font_types = false;
		}
		$default_font_types = $this->default_font_types;

		// Add font type variables (@font-serif, @font-sans-serif, @font-slab-serif, etc...)
		foreach ($default_font_types as $key => $value) {
			if( $active_font_types[ $value['id'] ]['family'] ){
				$less[ 'font-'.$value['id'] ] = $active_font_types[ $value['id'] ]['family'];
			} else {
				$less[ 'font-'.$value['id'] ] = $default_font_types[ $key ]['default-family'];
			}
		}

		//////////////////////////////////////////////////////////////////////////////////////////////////////
		// Color variables
		//////////////////////////////////////////////////////////////////////////////////////////////////////
		
		$less['color-primary-contrast'] = 'contrast(@color-primary, @color-dark, @color-light)';
		$less['color-secondary-contrast'] = 'contrast(@color-secondary, @color-dark, @color-light)';
		$less['color-tertiary-contrast'] = 'contrast(@color-tertiary,  @color-dark, @color-light)';
		$less['color-accent1-contrast'] = 'contrast(@color-accent1,   @color-dark, @color-light)';
		$less['color-accent2-contrast'] = 'contrast(@color-accent2,   @color-dark, @color-light)';

		// Adding a custom LESS var, use this in LESS as @my-var. In this example, its linked to a custom color picker in options. We also must set a default or else it's going to error.
		// pl_hashify must be used with color pickers so that it appends the # symbol to the hex code
		// pl_setting is being used because this is a global option used in the theme

		$less['psuedoContent'] = "\"\"";

		foreach ($this->color_data['variables'] as $var => $value) {
			$less[$var] = $value;
		}
		$less['color-light'] = 'mix(@color-primary, #fff, 10%)';
		$less['color-dark']  = 'mix(@color-primary, #000, 15%)';

		$less['color-white'] = '#fff';
		$less['color-black'] = '#000';
		
		$less['themeColor1'] = '@color-primary';
		$less['themeColor2'] = '@color-secondary';
		$less['themeColor3'] = '@color-tertiary';
		$less['themeColor4'] = '@color-accent1';
		$less['themeColor5'] = '@color-accent2';


		// Modular font scale variables
		// 
		if($sms_utils){
			$less['font-size-xxxxxlarge'] = ( $sms_utils->get_font_size_rem_from_key('xxxxxlarge') ?: '6.1rem' );
			$less['font-size-xxxxlarge']  = ( $sms_utils->get_font_size_rem_from_key('xxxxlarge')  ?: '4.9rem' );
			$less['font-size-xxxlarge']   = ( $sms_utils->get_font_size_rem_from_key('xxxlarge')   ?: '3.9rem' );
			$less['font-size-xxlarge']    = ( $sms_utils->get_font_size_rem_from_key('xxlarge')    ?: '3.1rem' );
			$less['font-size-xlarge']     = ( $sms_utils->get_font_size_rem_from_key('xlarge')     ?: '2.5rem' );
			$less['font-size-large']      = ( $sms_utils->get_font_size_rem_from_key('large')      ?: '2rem'   );
			$less['font-size-medium']     = ( $sms_utils->get_font_size_rem_from_key('medium')     ?: '1.6rem' );
			$less['font-size-small']      = ( $sms_utils->get_font_size_rem_from_key('small')      ?: '1.3rem' );
			$less['font-size-mini']       = ( $sms_utils->get_font_size_rem_from_key('mini')       ?: '1rem'   );

			$less['font-size-xxxxxlarge-px'] = ( $sms_utils->get_font_size_px_from_key('xxxxxlarge') ?: '61px' );
			$less['font-size-xxxxlarge-px']  = ( $sms_utils->get_font_size_px_from_key('xxxxlarge')  ?: '49px' );
			$less['font-size-xxxlarge-px']   = ( $sms_utils->get_font_size_px_from_key('xxxlarge')   ?: '39px' );
			$less['font-size-xxlarge-px']    = ( $sms_utils->get_font_size_px_from_key('xxlarge')    ?: '31px' );
			$less['font-size-xlarge-px']     = ( $sms_utils->get_font_size_px_from_key('xlarge')     ?: '25px' );
			$less['font-size-large-px']      = ( $sms_utils->get_font_size_px_from_key('large')      ?: '20px' );
			$less['font-size-medium-px']     = ( $sms_utils->get_font_size_px_from_key('medium')     ?: '16px' );
			$less['font-size-small-px']      = ( $sms_utils->get_font_size_px_from_key('small')      ?: '13px' );
			$less['font-size-mini-px']       = ( $sms_utils->get_font_size_px_from_key('mini')       ?: '10px' );
			
			$font_scale_base_int = intval( $sms_redux['font-scale-base'] );
			$font_scale_base_px = $sms_redux['font-scale-base'].'px';
			$font_scale_base = round((100/16)*$font_scale_base_int, 2).'%';
			$less['font-scale-base-percentage'] = $font_scale_base ?: "100%";
			$less['font-scale-base-rem'] = $font_scale_base_int ? ($font_scale_base_int * .1).'rem' : '1.6rem';
			$less['font-scale-base-px'] = $font_scale_base_int ? $font_scale_base_px : '16px';

			$less['font-size-h1'] = ( $sms_utils->get_font_size_em_from_key( $sms_utils->global_selected_font_kit_meta['stock-h1-assignment'] ) ?: '@font-size-xxxlarge' );
			$less['font-size-h2'] = ( $sms_utils->get_font_size_em_from_key( $sms_utils->global_selected_font_kit_meta['stock-h2-assignment'] ) ?: '@font-size-xxlarge'  );
			$less['font-size-h3'] = ( $sms_utils->get_font_size_em_from_key( $sms_utils->global_selected_font_kit_meta['stock-h3-assignment'] ) ?: '@font-size-xlarge'   );
			$less['font-size-h4'] = ( $sms_utils->get_font_size_em_from_key( $sms_utils->global_selected_font_kit_meta['stock-h4-assignment'] ) ?: '@font-size-large'    );
			$less['font-size-h5'] = ( $sms_utils->get_font_size_em_from_key( $sms_utils->global_selected_font_kit_meta['stock-h5-assignment'] ) ?: '@font-size-medium'   );
			$less['font-size-h6'] = ( $sms_utils->get_font_size_em_from_key( $sms_utils->global_selected_font_kit_meta['stock-h6-assignment'] ) ?: '@font-size-small'    );
		}
		
		// Adding path for child theme directory. Handy for importing LESS in the style.css.
		// Wrap in quotes so it's processed as a string by the compiler
		$less['childThemePath'] = sprintf( "\"%s\"", get_stylesheet_directory() );
		$less['childThemeURL'] = sprintf( "\"%s\"", get_stylesheet_directory_uri() );
		
		$less['pluginsURL'] = sprintf( "\"%s\"", WP_PLUGIN_URL );
		$less['pluginsPath'] = sprintf( "\"%s\"", WP_PLUGIN_DIR );

		$less['siteURL'] = sprintf( "\"%s\"", home_url() );

		// $endTime = "Time:  " . number_format(( microtime(true) - $this->startTime), 4) . " Seconds\n";
		// update_option('skeleton_benchmark', $endTime);

		return $less;
	}



	// Add a code widget to the head
	function register_widgets() {
		$head_area_widget = array(
			'id' => 'head-area',
			'name' => '<head>',
			'description' => 'WARNING: Only use the Code widget in this area. Normal widgets break page validation. Widgets appear directly in the <head>.',
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => ''
		);

		/* Register the sidebar. */
		register_sidebar( $head_area_widget );
	}

	public function get_colors(){

		global $sms_redux;
		global $sms_utils;

		$color_scheme_meta = ( $sms_utils->global_selected_color_scheme_meta ? $sms_utils->global_selected_color_scheme_meta : array() );

		$colors = array(
			'primary'   => $color_scheme_meta['def-color-primary'] ? $color_scheme_meta['def-color-primary'] : '#CFF09E',
			'secondary' => $color_scheme_meta['def-color-secondary'] ? $color_scheme_meta['def-color-secondary'] : '#A8DBA8',
			'tertiary'  => $color_scheme_meta['def-color-tertiary'] ? $color_scheme_meta['def-color-tertiary'] : '#79BD9A',
			'accent1'   => $color_scheme_meta['def-color-accent1'] ? $color_scheme_meta['def-color-accent1'] : '#3B8686',
			'accent2'   => $color_scheme_meta['def-color-accent2'] ? $color_scheme_meta['def-color-accent2'] : '#0B486B',
			'dark'      => 'mix(@color-primary, #000, 15%)',
			'light'     => 'mix(@color-primary, #fff, 10%)',
		);

		$selector_keys = array(
			'default'        => array(
				'class-suffix' => '',
				'selector'     => '',
				'property'     => 'color',
			),
			'background'     => array(
				'class-suffix' => '-bg',
				'selector'     => '',
				'property'     => 'background-color',
			),
			'border'         => array(
				'class-suffix' => '-border',
				'selector'     => '',
				'property'     => 'border-color',
			),
			'link'           => array(
				'class-suffix' => '-a',
				'selector'     => ' a',
				'property'     => 'color',
			),
			'icon'           => array(
				'class-suffix' => '-i',
				'selector'     => ' i',
				'property'     => 'color',
			),
		);

		$shades = array('dark', 'light'); // dark or light
		$levels = 5; // of brightness

		$super_array = array();

		foreach ($colors as $name => $color) {
			foreach ($shades as $shade) {
				
				// Neutral color
				if($shade == $shades[0])
					$super_array['variables']['color-'.$name] = $color;

				for ($i=1; $i <= $levels; $i++) {
					if($shade == 'dark'){
						$color_start = 90;
						$color_scale = 14;
						$color_mix = "dark";
					} else {
						$color_start = 80;
						$color_scale = 13;
						$color_mix = "light";
					}
					$super_array['variables']['color-'.$name.'-'.$shade.$i] = 'mix( '.$color.', @color-'.$color_mix.', '.($color_start - ( $color_scale * $i ) ).'% )';
				}
			}
		}

		foreach ($colors as $name => $color) {
			foreach ($selector_keys as $selector) {
				foreach ($shades as $shade) {
					
					for ($i=0; $i <= $levels; $i++) {
						if( $i == 0 ){
							// Neutral color
							if($shade == $shades[0]){
								$less_var = '@color-'.$name;
								$super_array['less_output'][] = '.color-'.$name.$selector['class-suffix'].'{'.$selector['property'].':'.$less_var.'}';
								$super_array['less_output'][] = '.color-'.$name.$selector['class-suffix'].'i{'.$selector['property'].':'.$less_var.'!important}';
							}
						} else {
							$less_var = '@color-'.$name.'-'.$shade.$i;
							$super_array['less_output'][] = '.color-'.$name.'-'.$shade.$i.$selector['class-suffix'].'{'.$selector['property'].':'.$less_var.'}';
							$super_array['less_output'][] = '.color-'.$name.'-'.$shade.$i.$selector['class-suffix'].'i{'.$selector['property'].':'.$less_var.' !important}';
						}
					}

				}
			}
		}

		$super_array['info']['defaults'] = ( $sms_utils->global_selected_color_scheme_meta ? "false" : "true" );

		// Sample output

		/*
		$super_array = array(
			'less_output' => array(
				'0' => '.color-primary{color:@color-primary}',
				'1' => '.color-primaryi{color:@color-primary!important}',
				'2' => '.color-primary-dark1{color:@color-primary-dark1}',
				'3' => '.color-primary-dark1i{color:@color-primary-dark1 !important}',
				'4' => '...'
			),
			'variables' => array(
				'color-primary' => '#CFF09E',
				'color-primary-dark1' => 'mix( #CFF09E, @color-dark, 76% )',
				'color-primary-dark2' => '...'
			),
			'info' => array(
				'defaults' = false
			),
		)
		*/
		return $super_array;

	}

	// Draw Welcome Panel
	function welcome(){

		ob_start();
		?>
			 <div style="font-size:14px;line-height:14px;color:#444;">
				<p><?php _e('Welcome to DMS Skeleton Theme! ','dms_skeleton');?></p>
			</div>
			</div>
			<div style="margin-top:20px;color:#444;">
				<p style="border-bottom:1px solid #ccc;margin:0 0 0.75em;"><strong><?php _e('Overview','dms_skeleton');?></strong></p>
				<p style="font-size:12px;line-height:14px;"><?php _e('DMS Skeleton is designed to work with core PageLines sections.','dms_skeleton');?></p>
			</div>
		<?php
		return ob_get_clean();
	}

	// Theme Options
	function options(){
		$theme_settings = array();
	}


}

$dms_skeleton_theme = new dmsSkeletonTheme;

add_action('admin_init', function(){
	global $dms_skeleton_theme;
	// $color_less_output = $dms_skeleton_theme->custom_less_output();
	// echo "<pre>\$color_less_output: " . print_r($color_less_output, true) . "</pre>";
	// echo "<pre>\$color_less_output: " . print_r($color_less_output, true) . "</pre>";

	// $color_less_vars = $dms_skeleton_theme->custom_less_vars();
	// echo "<pre>\$color_less_vars: " . print_r($color_less_vars, true) . "</pre>";
});



add_filter( 'render_css_posix_', '__return_true' ); //Flywheel Support


