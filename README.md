# THEME NOTES

To see a full list of custom classes available for the theme, please check out the style.less file.

### Responsive Typography Standards ###

Mobile font sizes are set to PX, so they don't scale. Here's the default size mapping. 

* H1: Large
* H2: Medium
* H3: Medium Bold
* H4: Medium
* H5: Small
* H6: Small
* Primary Headings: xxlarge
* Secondary Headings: xlarge Bold
* Tertiary Headings: Large semi-bold (300)

Footer

* Primary Headings: 22px
* Secondary Headings: 20px
* Tertiary Headings: 18px

### Font Classes ###

* ```fs-xxxxxlarge``` or ```fs-7x```
* ```fs-xxxxlarge``` or ```fs-6x```
* ```fs-xxxlarge``` or ```fs-5x```
* ```fs-xxlarge``` or ```fs-4x```
* ```fs-xlarge``` or ```fs-3x```
* ```fs-large``` or ```fs-2x```
* ```fs-medium``` or ```fs-1x```
* ```fs-small```
* ```fs-mini```

To manually override the font sizes on mobile, you can use the following classes

* ```mfs-xxxxxlarge``` or ```mfs-7x```
* ```mfs-xxxxlarge``` or ```mfs-6x```
* ```mfs-xxxlarge``` or ```mfs-5x```
* ```mfs-xxlarge``` or ```mfs-4x```
* ```mfs-xlarge``` or ```mfs-3x```
* ```mfs-large``` or ```mfs-2x```
* ```mfs-medium``` or ```mfs-1x```
* ```mfs-small```
* ```mfs-mini```


### Less variables ###

#### Font Sizes ####

* ```@font-size-xxxxxlarge```
* ```@font-size-xxxxlarge```
* ```@font-size-xxxlarge```
* ```@font-size-xxlarge```
* ```@font-size-xlarge```
* ```@font-size-large```
* ```@font-size-medium```
* ```@font-size-small```
* ```@font-size-mini```

* ```@font-size-xxxxxlarge-px```
* ```@font-size-xxxxlarge-px```
* ```@font-size-xxxlarge-px```
* ```@font-size-xxlarge-px```
* ```@font-size-xlarge-px```
* ```@font-size-large-px```
* ```@font-size-medium-px```
* ```@font-size-small-px```
* ```@font-size-mini-px```


#### Colors ####

* ```@color-primary```
* ```@color-secondary```
* ```@color-tertiary```
* ```@color-accent1```
* ```@color-accent2```

```@color-light```
Derived by mixing 10% of the primary color into white

```@color-dark```
Derived by mixing 15% of the primary color into black

Each of these returns either @color-light or @color-dark depending on the contrast.

* ```@color-primary-contrast```
* ```@color-secondary-contrast```
* ```@color-tertiary-contrast```
* ```@color-accent1-contrast```
* ```@color-accent2-contrast```
