<?php
// For adding dynamic copywrite date in content fields
function display_current_year($atts, $content = null) {
  return date("Y");
}
add_shortcode("the_year", "display_current_year");

add_shortcode('sms_site_search', 'sms_display_search');
function sms_display_search($atts) {

  ob_start();
    echo '<div class="site-search">';
    pagelines_search_form(true);
    echo '</div>';
  return ob_get_clean();

}

// Supports all arguments of wp_nav_menu() function
// ------
// Examples:
//
// Display Custom Menu
// [custom_menu menu='Primary Menu' depth='1']
//
// Limit depth to the first level
// [custom_menu menu='Primary Menu' depth='1']

// Display custom menu with font-awesome icons
// [custom_menu menu='Primary Menu' depth='1' menu_class="fa-ul" before="<i class='fa-li fa fa-check-square'></i>" ]
//
// menu: (string) (optional) The menu that is desired; accepts (matching in order) id, slug, name

// Function that will return our Wordpress menu
if(!function_exists("custom_menu_shortcode")){
  function custom_menu_shortcode($atts, $content = null) {
    extract(shortcode_atts(array(
      'menu'            => '',
      'container'       => 'false',
      'container_class' => '',
      'container_id'    => '',
      'menu_class'      => 'custom_menu',
      'menu_id'         => '',
      'echo'            => true,
      'fallback_cb'     => 'wp_page_menu',
      'before'          => '',
      'after'           => '',
      'link_before'     => '',
      'link_after'      => '',
      'depth'           => 0,
      'walker'          => '',
      'theme_location'  => ''),
      $atts));

    return wp_nav_menu( array(
      'menu'            => $menu,
      'container'       => $container,
      'container_class' => $container_class,
      'container_id'    => $container_id,
      'menu_class'      => $menu_class,
      'menu_id'         => $menu_id,
      'echo'            => false,
      'fallback_cb'     => $fallback_cb,
      'before'          => $before,
      'after'           => $after,
      'link_before'     => $link_before,
      'link_after'      => $link_after,
      'depth'           => $depth,
      'walker'          => $walker,
      'theme_location'  => $theme_location));
  }
  //Create the shortcode
  add_shortcode("custom_menu", "custom_menu_shortcode");
}

add_shortcode('sms_color_preview','display_sms_color_preview');
function display_sms_color_preview( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'attribute1' => 'default_value',
  ), $atts ) );

  global $dms_skeleton_theme;
  $color_data = $dms_skeleton_theme->get_colors();

  $ob_str = '';

  if( $color_data['info']['defaults'] == "true" ){
    $ob_str .= '<h3>Using default colors</h3>';
    $ob_str .= '<p>Make sure the SMS plugin is activated, and you\'ve got a color scheme selected and filled out.</p>';
  }
  
  ob_start();
  ?>
  <div class="row margin-bottom">

    <div class="span4">

      <h4 class="align-center margin-bottom color-primary-bg color-primary-contrast padding border-radius clear">Primary</h4>

      <div class="color-palette-column">
      
        <div class="color-palette-block color-dark color-primary-light5-bg"><span>color-primary-light5</span></div>
        <div class="color-palette-block color-dark color-primary-light4-bg"><span>color-primary-light4</span></div>
        <div class="color-palette-block color-dark color-primary-light3-bg"><span>color-primary-light3</span></div>
        <div class="color-palette-block color-dark color-primary-light2-bg"><span>color-primary-light2</span></div>
        <div class="color-palette-block color-dark color-primary-light1-bg"><span>color-primary-light1</span></div>

        <div class="color-palette-block color-primary-bg color-primary-contrast"><span class="color-primary-contrast">color-primary</span></div>

        <div class="color-palette-block color-light color-primary-dark1-bg"><span>color-primary-dark1</span></div>
        <div class="color-palette-block color-light color-primary-dark2-bg"><span>color-primary-dark2</span></div>
        <div class="color-palette-block color-light color-primary-dark3-bg"><span>color-primary-dark3</span></div>
        <div class="color-palette-block color-light color-primary-dark4-bg"><span>color-primary-dark4</span></div>
        <div class="color-palette-block color-light color-primary-dark5-bg"><span>color-primary-dark5</span></div>
      </div>

      <div class="color-palette-column">
        <div class="color-palette-block color-primary-light5"><span>color-primary-light5</span></div>
        <div class="color-palette-block color-primary-light4"><span>color-primary-light4</span></div>
        <div class="color-palette-block color-primary-light3"><span>color-primary-light3</span></div>
        <div class="color-palette-block color-primary-light2"><span>color-primary-light2</span></div>
        <div class="color-palette-block color-primary-light1"><span>color-primary-light1</span></div>

        <div class="color-palette-block"><span class="color-primary">color-primary</span></div>

        <div class="color-palette-block color-primary-dark1"><span>color-primary-dark1</span></div>
        <div class="color-palette-block color-primary-dark2"><span>color-primary-dark2</span></div>
        <div class="color-palette-block color-primary-dark3"><span>color-primary-dark3</span></div>
        <div class="color-palette-block color-primary-dark4"><span>color-primary-dark4</span></div>
        <div class="color-palette-block color-primary-dark5"><span>color-primary-dark5</span></div>
      </div>
    </div>

    <div class="span4">
      
      <h4 class="align-center margin-bottom color-secondary-bg color-secondary-contrast padding border-radius clear">Secondary</h4>

      <div class="color-palette-column">

        <div class="color-palette-block color-dark color-secondary-light5-bg"><span>color-secondary-light5</span></div>
        <div class="color-palette-block color-dark color-secondary-light4-bg"><span>color-secondary-light4</span></div>
        <div class="color-palette-block color-dark color-secondary-light3-bg"><span>color-secondary-light3</span></div>
        <div class="color-palette-block color-dark color-secondary-light2-bg"><span>color-secondary-light2</span></div>
        <div class="color-palette-block color-dark color-secondary-light1-bg"><span>color-secondary-light1</span></div>

        <div class="color-palette-block color-secondary-bg"><span class="color-secondary-contrast">color-secondary</span></div>

        <div class="color-palette-block color-light color-secondary-dark1-bg"><span>color-secondary-dark1</span></div>
        <div class="color-palette-block color-light color-secondary-dark2-bg"><span>color-secondary-dark2</span></div>
        <div class="color-palette-block color-light color-secondary-dark3-bg"><span>color-secondary-dark3</span></div>
        <div class="color-palette-block color-light color-secondary-dark4-bg"><span>color-secondary-dark4</span></div>
        <div class="color-palette-block color-light color-secondary-dark5-bg"><span>color-secondary-dark5</span></div>
      </div>

      <div class="color-palette-column">
        <div class="color-palette-block color-secondary-light5"><span>color-secondary-light5</span></div>
        <div class="color-palette-block color-secondary-light4"><span>color-secondary-light4</span></div>
        <div class="color-palette-block color-secondary-light3"><span>color-secondary-light3</span></div>
        <div class="color-palette-block color-secondary-light2"><span>color-secondary-light2</span></div>
        <div class="color-palette-block color-secondary-light1"><span>color-secondary-light1</span></div>

        <div class="color-palette-block"><span class="color-secondary">color-secondary</span></div>

        <div class="color-palette-block color-secondary-dark1"><span>color-secondary-dark1</span></div>
        <div class="color-palette-block color-secondary-dark2"><span>color-secondary-dark2</span></div>
        <div class="color-palette-block color-secondary-dark3"><span>color-secondary-dark3</span></div>
        <div class="color-palette-block color-secondary-dark4"><span>color-secondary-dark4</span></div>
        <div class="color-palette-block color-secondary-dark5"><span>color-secondary-dark5</span></div>
      </div>
    </div>

    <div class="span4">

      <h4 class="align-center margin-bottom color-tertiary-bg color-tertiary-contrast padding border-radius clear">Tertiary</h4>

      <div class="color-palette-column">
        <div class="color-palette-block color-dark color-tertiary-light5-bg"><span>color-tertiary-light5</span></div>
        <div class="color-palette-block color-dark color-tertiary-light4-bg"><span>color-tertiary-light4</span></div>
        <div class="color-palette-block color-dark color-tertiary-light3-bg"><span>color-tertiary-light3</span></div>
        <div class="color-palette-block color-dark color-tertiary-light2-bg"><span>color-tertiary-light2</span></div>
        <div class="color-palette-block color-dark color-tertiary-light1-bg"><span>color-tertiary-light1</span></div>

        <div class="color-palette-block color-tertiary-bg"><span class="color-tertiary-contrast">color-tertiary</span></div>

        <div class="color-palette-block color-light color-tertiary-dark1-bg"><span>color-tertiary-dark1</span></div>
        <div class="color-palette-block color-light color-tertiary-dark2-bg"><span>color-tertiary-dark2</span></div>
        <div class="color-palette-block color-light color-tertiary-dark3-bg"><span>color-tertiary-dark3</span></div>
        <div class="color-palette-block color-light color-tertiary-dark4-bg"><span>color-tertiary-dark4</span></div>
        <div class="color-palette-block color-light color-tertiary-dark5-bg"><span>color-tertiary-dark5</span></div>
      </div>

      <div class="color-palette-column">
        <div class="color-palette-block color-tertiary-light5"><span>color-tertiary-light5</span></div>
        <div class="color-palette-block color-tertiary-light4"><span>color-tertiary-light4</span></div>
        <div class="color-palette-block color-tertiary-light3"><span>color-tertiary-light3</span></div>
        <div class="color-palette-block color-tertiary-light2"><span>color-tertiary-light2</span></div>
        <div class="color-palette-block color-tertiary-light1"><span>color-tertiary-light1</span></div>

        <div class="color-palette-block"><span class="color-tertiary">color-tertiary</span></div>

        <div class="color-palette-block color-tertiary-dark1"><span>color-tertiary-dark1</span></div>
        <div class="color-palette-block color-tertiary-dark2"><span>color-tertiary-dark2</span></div>
        <div class="color-palette-block color-tertiary-dark3"><span>color-tertiary-dark3</span></div>
        <div class="color-palette-block color-tertiary-dark4"><span>color-tertiary-dark4</span></div>
        <div class="color-palette-block color-tertiary-dark5"><span>color-tertiary-dark5</span></div>
      </div>
    </div>

  </div>


  <div class="row">

    <div class="span4">

      <h4 class="align-center margin-bottom color-accent1-bg color-accent1-contrast padding border-radius clear">Accent1</h4>

      <div class="color-palette-column">
        <div class="color-palette-block color-dark color-accent1-light5-bg"><span>color-accent1-light5</span></div>
        <div class="color-palette-block color-dark color-accent1-light4-bg"><span>color-accent1-light4</span></div>
        <div class="color-palette-block color-dark color-accent1-light3-bg"><span>color-accent1-light3</span></div>
        <div class="color-palette-block color-dark color-accent1-light2-bg"><span>color-accent1-light2</span></div>
        <div class="color-palette-block color-dark color-accent1-light1-bg"><span>color-accent1-light1</span></div>

        <div class="color-palette-block color-accent1-bg"><span class="color-accent1-contrast">color-accent1</span></div>

        <div class="color-palette-block color-light color-accent1-dark1-bg"><span>color-accent1-dark1</span></div>
        <div class="color-palette-block color-light color-accent1-dark2-bg"><span>color-accent1-dark2</span></div>
        <div class="color-palette-block color-light color-accent1-dark3-bg"><span>color-accent1-dark3</span></div>
        <div class="color-palette-block color-light color-accent1-dark4-bg"><span>color-accent1-dark4</span></div>
        <div class="color-palette-block color-light color-accent1-dark5-bg"><span>color-accent1-dark5</span></div>
      </div>

      <div class="color-palette-column">
        <div class="color-palette-block color-accent1-light5"><span>color-accent1-light5</span></div>
        <div class="color-palette-block color-accent1-light4"><span>color-accent1-light4</span></div>
        <div class="color-palette-block color-accent1-light3"><span>color-accent1-light3</span></div>
        <div class="color-palette-block color-accent1-light2"><span>color-accent1-light2</span></div>
        <div class="color-palette-block color-accent1-light1"><span>color-accent1-light1</span></div>

        <div class="color-palette-block"><span class="color-accent1">color-accent1</span></div>

        <div class="color-palette-block color-accent1-dark1"><span>color-accent1-dark1</span></div>
        <div class="color-palette-block color-accent1-dark2"><span>color-accent1-dark2</span></div>
        <div class="color-palette-block color-accent1-dark3"><span>color-accent1-dark3</span></div>
        <div class="color-palette-block color-accent1-dark4"><span>color-accent1-dark4</span></div>
        <div class="color-palette-block color-accent1-dark5"><span>color-accent1-dark5</span></div>
      </div>

    </div>

    <div class="span4">

      <h4 class="align-center margin-bottom color-accent2-bg color-accent2-contrast padding border-radius clear">Accent #2</h4>

      <div class="color-palette-column">
        <div class="color-palette-block color-dark color-accent2-light5-bg"><span>color-accent2-light5</span></div>
        <div class="color-palette-block color-dark color-accent2-light4-bg"><span>color-accent2-light4</span></div>
        <div class="color-palette-block color-dark color-accent2-light3-bg"><span>color-accent2-light3</span></div>
        <div class="color-palette-block color-dark color-accent2-light2-bg"><span>color-accent2-light2</span></div>
        <div class="color-palette-block color-dark color-accent2-light1-bg"><span>color-accent2-light1</span></div>

        <div class="color-palette-block color-accent2-bg"><span class="color-accent2-contrast">color-accent2</span></div>

        <div class="color-palette-block color-light color-accent2-dark1-bg"><span>color-accent2-dark1</span></div>
        <div class="color-palette-block color-light color-accent2-dark2-bg"><span>color-accent2-dark2</span></div>
        <div class="color-palette-block color-light color-accent2-dark3-bg"><span>color-accent2-dark3</span></div>
        <div class="color-palette-block color-light color-accent2-dark4-bg"><span>color-accent2-dark4</span></div>
        <div class="color-palette-block color-light color-accent2-dark5-bg"><span>color-accent2-dark5</span></div>
      </div>

      <div class="color-palette-column">
        <div class="color-palette-block color-accent2-light5"><span>color-accent2-light5</span></div>
        <div class="color-palette-block color-accent2-light4"><span>color-accent2-light4</span></div>
        <div class="color-palette-block color-accent2-light3"><span>color-accent2-light3</span></div>
        <div class="color-palette-block color-accent2-light2"><span>color-accent2-light2</span></div>
        <div class="color-palette-block color-accent2-light1"><span>color-accent2-light1</span></div>

        <div class="color-palette-block"><span class="color-accent2">color-accent2</span></div>

        <div class="color-palette-block color-accent2-dark1"><span>color-accent2-dark1</span></div>
        <div class="color-palette-block color-accent2-dark2"><span>color-accent2-dark2</span></div>
        <div class="color-palette-block color-accent2-dark3"><span>color-accent2-dark3</span></div>
        <div class="color-palette-block color-accent2-dark4"><span>color-accent2-dark4</span></div>
        <div class="color-palette-block color-accent2-dark5"><span>color-accent2-dark5</span></div>
      </div>
    </div>


    <div class="span4 color-palette-scrunch">

      <h4 class="align-center margin-bottom color-dark-bg color-light padding border-radius clear">Light and dark shades</h4>

      <div class="color-palette-column">
        <div class="color-palette-block color-light-bg"><span class="color-dark">color-light</span></div>
        <div class="color-palette-block color-light color-light-dark1-bg"><span>color-light-dark1</span></div>
        <div class="color-palette-block color-light color-light-dark2-bg"><span>color-light-dark2</span></div>
        <div class="color-palette-block color-light color-light-dark3-bg"><span>color-light-dark3</span></div>
        <div class="color-palette-block color-light color-light-dark4-bg"><span>color-light-dark4</span></div>
        <div class="color-palette-block color-light color-light-dark5-bg"><span>color-light-dark5</span></div>
      </div>
      <div class="color-palette-column">
        <div class="color-palette-block"><span class="color-light">color-light</span></div>
        <div class="color-palette-block color-light-dark1"><span>color-light-dark1</span></div>
        <div class="color-palette-block color-light-dark2"><span>color-light-dark2</span></div>
        <div class="color-palette-block color-light-dark3"><span>color-light-dark3</span></div>
        <div class="color-palette-block color-light-dark4"><span>color-light-dark4</span></div>
        <div class="color-palette-block color-light-dark5"><span>color-light-dark5</span></div>
      </div>
      <div class="color-palette-column">
        <div class="color-palette-block color-dark-bg"><span class="color-light">color-dark</span></div>
        <div class="color-palette-block color-dark color-dark-light1-bg"><span>color-dark-light1</span></div>
        <div class="color-palette-block color-dark color-dark-light2-bg"><span>color-dark-light2</span></div>
        <div class="color-palette-block color-dark color-dark-light3-bg"><span>color-dark-light3</span></div>
        <div class="color-palette-block color-dark color-dark-light4-bg"><span>color-dark-light4</span></div>
        <div class="color-palette-block color-dark color-dark-light5-bg"><span>color-dark-light5</span></div>
      </div>
      <div class="color-palette-column">
        <div class="color-palette-block"><span class="color-dark">color-dark</span></div>
        <div class="color-palette-block color-dark-light1"><span>color-dark-light1</span></div>
        <div class="color-palette-block color-dark-light2"><span>color-dark-light2</span></div>
        <div class="color-palette-block color-dark-light3"><span>color-dark-light3</span></div>
        <div class="color-palette-block color-dark-light4"><span>color-dark-light4</span></div>
        <div class="color-palette-block color-dark-light5"><span>color-dark-light5</span></div>
      </div>

    </div>

  </div>
  <?php
  $ob_str .= ob_get_contents();
  ob_end_clean();
  return $ob_str;
}

add_shortcode('sms_font_preview','display_sms_font_preview');
function display_sms_font_preview( $atts, $content = null ) {
  extract( shortcode_atts( array(
    'attribute1' => 'default_value',
  ), $atts ) );
  global $sms_utils;
  if(!$sms_utils)
    return "<p>SMS Is not installed, or the data's not being stored correctly. Unable to use \$sms_utils->get_font_family_from_font_type</p>";
  
  $panel_classes = array( 'color-contrast', 'color-lighti color-dark-bg', 'color-darki color-light-bg' );
  $font_type_data = $sms_utils->get_all_font_type_data();
  $font_type_active_list = $sms_utils->get_font_type_list_as_redux_select_array();

  // echo "<pre style='font-size: 14px; line-height: 16px;'>\$font_type_data: " . print_r($font_type_data, true) . "</pre>";
  // echo "<pre style='font-size: 14px; line-height: 16px;'>\$font_type_active_list: " . print_r($font_type_active_list, true) . "</pre>";
  
  $output = '';
  
  $output .= "[pl_accordion name='".$key."']";
  $i = 1;
  foreach ($font_type_active_list as $key => $title) {

    $font_family = $sms_utils->global_selected_font_kit_meta[ $key .'-font-family' ];

    // echo "<pre>\$key: " . print_r($key, true) . "</pre>";
    // echo "<pre>\$font_family: " . print_r($font_family, true) . "</pre>";
    // echo "<pre>\$default_font_family: " . print_r($font_type_data[$key]['default-font-family'], true) . "</pre>";
    
    // $font_kit_fields = $sms_utils->sms_options['fonts']['definition-field-data'];
    // echo "<pre>\$font_kit_fields: " . print_r($font_kit_fields, true) . "</pre>";



    $output .= "[pl_accordioncontent name='$key number='$i' heading='$title' {".$sms_utils->get_font_family_from_font_type($key)."}]";

    $j = 1;
    foreach ($panel_classes as $panel_class) {

      // $output_info = '';
      // $output_info .= "<ul class='font-sans-serif color-dark'>";
      // $output_info .= "<li>Font Type: ".$font_type_active_list[$key]."</li>";
      // $output_info .= "<li><strong>CSS class:</strong>font-$key</li>";
      // $output_info .= "<li><strong>font family: </strong>".$sms_utils->get_font_family_from_font_type($key)."</li>";
      // $output_info .= "</ul>";

      $output .= "<div class='$panel_class padding border-radius'>";
      $output .= '  <div class="font-preview-wrapper">';
      $output .= "    <h1 class='heading--primary font-$key'>(".$font_type_active_list[$key].") The quick brown fox jumps over the lazy dog</h1>";
      $output .= "    <h2 class='heading--secondary font-$key'>(".$font_type_active_list[$key].") The quick brown fox jumps over the lazy dog</h2>";
      $output .= "    <h3 class='heading--tertiary font-$key'>The quick brown fox jumps over the lazy dog</h3>";
      // $output .= '    [pl_popover title="Font Data" content="'.$output_info.'" position="right"]Get more info[/pl_popover]';
      $output .= "    <p class='font-$key'>The quick brown fox jumps over the lazy dog. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo   consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non   proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>";
      // $output .= "    <p class='font-$key'>$sms_utils->get_font_family_from_font_type($key)</p>";
      $output .= "  </div>";
      $output .= "</div>";
      // $output .= "<hr class='margin-vertical-large'>";
      $j++;
    }
    $output .= "[/pl_accordioncontent]";
    $i++;
  }
  $output .= "[/pl_accordion]";
  return $output;
}
