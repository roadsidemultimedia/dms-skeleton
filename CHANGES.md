### Changelog

#### 0.6.9 [01/08/2015]

* Added CSS to disable IOS default grey highlight behavior
* Added styling for ```input[type="tel"]``` for forms on dark bands.
* Added [sms_site_search] shortcode to display a search field.
* Class added to justify left text on columns for tablet screens and larger: ```.left-justify-tablet-up```, and it's alias class ```.left-align-tablet-up```
* Class added to center justify text on columns for mobile screens: ```.center-justify-mobile```, and it's alias class ```.center-align-mobile```

#### 0.6.8 [11/16/2014]

* Removed Flex slider JS, since it's not used.
* Added option for custom band ID, used for scrolling script.
* ```no-side-padding``` class added for removing padding from side when viewed at smaller screen sizes.
* Added font size classes that apply only to tablet portrait size and smaller (See readme.md)
* Cleaned up unused CSS
* Added optimized versions of pl.common.js and pl.helpers.js
    

#### 0.6.7 [11/12/2014]

* Adjusted layout classes that remove padding so they have ```!important``` tag and actually work correctly.
* Removed css that caused a tags to have an opacity of 0.7 on hover.
* Added function to prevent images in blog from being wrapped in P tags. This fixes layout issues when an image is placed at the top of a blog post.

#### 0.6.6 [10/16/2014]

* Added option to set canvas to be full width with a checkbox.
* Added button to front end band options that links you to the Band style backend page. (Requires SMS 1.4.5)


#### 0.6.5 [10/15/2014]

* Added option to canvas area to set to full window height. This also vertically centers the content.

#### 0.6.4 [10/14/2014]

* **(Less cache flush required)** Revamped styling of gravity forms fields: email, number, multiple choice, select dropdown, checkbox, and radio. 
* Added utility classes for controlling the font scale of a container on smaller screens.

#### 0.6.3 [09/10/2014]

* Additional utility classes for positioning. pos-rel and pos-abs for relative and absolute positioning.
* .noise class adds an overlay of light noise over any element
* Moved some utility color classes out of stock DMS less overrides and into Skeleton child theme specific less. It seems that the /less/colors.less file is included into core and section, and should only contain variables.

#### 0.6.2 [08/19/2014]

* Fixed a tiny syntax error in variables.less that may have prevented compilation of LESS.

#### 0.6.1 [08/18/2014]

* Added EM based utility classes for margin. Used by version 

#### 0.6.0 [08/13/2014]

* Converted almost all static font units to relative units. Many many many updates and edits.
    * Font size of html element is 10px to reset the font scale to multiples of 10. This makes rem units equal pixels / 10. 
    * The body size is set to a px value using the base font size as set in the global font kit options.
    * All other values are rem and em based off of these. 
    * Media queries cause the html size and body size to scale down the fonts
    * An example of this in action can be found here: http://codepen.io/rsmm/pen/gkJBj
* H1-H6 sizes now mapped to variables set in SMS
* Revolution slider is no longer hidden by default on mobile devices
* Fixed extra padding weirdness with textboxes when viewed on small screens
* Updated fix for WP Engine cloning issue. Checks if directory matches, rather than checking if the file exists.

#### 0.5.2 [07/31/2014]

* Added default font sizes for heading, subheading and accent heading.
* Added default font color for light and dark bands (```@color-light``` & ```@color-dark```)
* Set several LESS vars to use SMS colors and fonts.
* Added psuedo classes for font sizes (thanks for Ryan's suggestion)
    * ```.fs-large``` = ```.fs-2x```
    * ```.fs-xlarge``` = ```.fs-3x```
    * ```.fs-xxlarge``` = ```.fs-4x```

#### 0.5.1 [07/23/2014]

 * Added variables and classes for modular font scale (.font-size-mini, .font-size-small, .font-size-medium, etc...)
 * Added default line height of 1.2x for font size classes.
 * Overwriting base font size variable ```@baseFontSize``` with ```@font-size-medium```
 * Moved font type classes to LESS file
 * Moved color contrast classes to LESS file
 * Moved variables.less from parent theme to child theme
 * Added Bitbucket repo reference to style.css for auto updates

#### 0.5.0

 * Starting to keep a changelog.
 * Added Color System 2.0
 * Added font preview shortcode [sms_font_preview]
 * Added color preview shortcode [sms_color_preview]
 * Added a fix for WP Engine cloning process taht resulted in a 502 bad gateway caused by database tables storing static section paths. They are now cleared if the paths don't match the current install.
 * Added LESS variabels for font types
   * .font-sans-serif, .serif, etc... (see [docs](http://confluence.roadside2.com/display/DEVPROD/DMS+Skeleton+CSS+Classes))
 * Renamed color classes for black and white opacities to conform to the rest of the classnames for colors.
 * Removed theme options that are now handled by Redux
 * Removed unused classes & variable references
